# Messröhren für das Langzeitarchiv:				

| #	| Nomen | SPS-Name | SPS | Accelerator Zone | Accelerator |SIS | ESR | HEST | Cryring | UNILAC | FESA Integration now |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 1	| GS01VM1G | sQR06BPX06 | BELCPVACS18 | SIS18_RING | SIS18 | x |  |  |  |  | x |
| 2	| GS02VM1G	| sQR07BPX03 | BELCPVACS18 | SIS18_RING | SIS18 | x |  |  |  |  | x |
| 3	| GS03VM1G	| sQR07BPX06 | BELCPVACS18 | SIS18_RING | SIS18 | x |  |  |  |  | x |
| 4	| GS04VM1G	| sQR01BPX03 | BELCPVACS18 | SIS18_RING | SIS18 | x |  |  |  |  | x |
| ... |

